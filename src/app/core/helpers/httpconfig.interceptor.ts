import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (
      localStorage.getItem('currentUser') != null &&
      localStorage.getItem('currentUser') != ''
    ) {

      // const userData = JSON.parse(localStorage.getItem('currentUser'));
      // console.log(userData);
      // const token = userData.access_token;
      // if (token) {
      //   request = request.clone({
      //     headers: request.headers.set('Authorization', `JWT ${token}`),
      //   });
      // }

      if (!request.headers.has('Content-Type')) {
        request = request.clone({
          headers: request.headers.set('Content-Type', 'application/json'),
        });
      }

      request = request.clone({
        headers: request.headers.set('Accept', 'application/json'),
      });
    }
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          console.log('event--->>>', event);
        }
        return event;
      })
    );
  }
}
