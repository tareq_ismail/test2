import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Location } from '@angular/common'
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  bulltPlaceHolder: any = `••••••••`;
    constructor(
        private location: Location,
        private router: Router
      ) {
      }

  backToPreviousPage(){
    this.location.back();
  }

  redirectTo(path,params=null){
    if(params)
      this.router.navigate([path,params]);
    else
      this.router.navigate([path]);
  }

  stringToBoolean(stringFlag){
    return stringFlag === 'true' || stringFlag === true;
  }

  setFocusOnElement(selector){
    setTimeout(() => {
      document.querySelector(selector).focus();
    });
  }

  extractName(f_name,l_name){
    if(f_name && l_name) return f_name + " " + l_name;
    else if(f_name) return f_name;
    else if(l_name) return l_name;
    else return "---";
  }

  scrollToBottom(){
    window.scrollTo({ top: document.body.scrollHeight, behavior: 'smooth' });
  }

  scrollToTop(){
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  removeFromArrayByIndex(arr,index){
    if(arr.length > 1){
      arr.splice(index, 1);
      return arr;
    }else{
      return [];
    }
  }

  getElementAspects(selector){
    let ele = document.querySelector(selector);
    return {
      width: ele.offsetWidth,
      height: ele.offsetHeight
    };
  }

  triggerOnElement(selector){
    document.querySelector(selector).click();
  }

  insertJSFileIntoDom(file_path){
    var script = document.createElement('script');
    script.src = file_path;
    document.head.appendChild(script);
  }

  leadZero(num){
    num = parseInt(num);
    if(num < 10) num = "0" + num;
    return num;
  }

  getShareLinks(url,text = "",sourceOfTweet = "",tumblr_name = ""){
    url = encodeURI(url); //encoded url..
    return {
      facebook: `https://www.facebook.com/sharer/sharer.php?u=${url}`,
      twitter: `https://twitter.com/share?text=${text}&via=${sourceOfTweet}&url=${url}`,
      linkedin: `https://www.linkedin.com/sharing/share-offsite/?url=${url}`,
      whatsapp: `https://api.whatsapp.com/send?text=${text} ${url}`,
      pinterest: `http://pinterest.com/pin/create/link/?url=${url}&media=${url}&description=${text}`,
      tumblr: `http://www.tumblr.com/share/link?url=${url}&name=${tumblr_name}&description=${text}`,
    };
  }

  buildShareURL(){
    return environment.shareLink + document.location.pathname;
  }

  copyToClipboard(txt){
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = txt;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  addClassToModalAtThisPage(new_class){
    setTimeout(() => {
      let ele_arr = document.getElementsByClassName("modal-dialog");
      for (let index = 0; index < ele_arr.length; index++) {
        ele_arr[index].classList.add(new_class);
      }
    });
  }

  isMob(){
    return window.innerWidth <= 768;
  }
  
  isTablet(){
    return window.innerWidth <= 991;
  }
  
  isDesktop(){
    return window.innerWidth >= 992;
  }
  
  removeDecimalPlaces(num){
    return ~~num;
  }

  isEmpty(value){
    return (
      // null or undefined
      (value == null) ||

      // has length and it's zero
      (value.hasOwnProperty('length') && value.length === 0) ||

      // is an Object and has no keys
      (value.constructor === Object && Object.keys(value).length === 0)
    )
  }

  printPage(){
    window.print();
  }

  busy(e){
    // document.querySelector("body").style.cursor = e ? "wait" : "auto";
  }

  getUserDetails(){
    // 
  }

  isLoggedIn(){
    // 
  }

  getLang(){
    // 
  }

  getCurrency(){
    // 
  }

  replaceString(str,oldStr,newStr,replaceAll = false){
    if(!(str.indexOf(oldStr) !== -1)) return str; //if string is not exist
    // replace if exist
    // why to check if string exist or not, cause if it not exist we will have a error.
    // 
    if(replaceAll) return str.replaceAll(oldStr, newStr);
    else return str.replace(oldStr, newStr);
  }

  getCountryList(){
    return [
      {
        flag: "https://restcountries.eu/data/afg.svg",
        countryCode: "+93",
        name: "Afghanistan",
        name_ar: "افغانستان"
      },
      {
        flag: "https://restcountries.eu/data/jor.svg",
        countryCode: "+962",
        name: "Jordan",
        name_ar: "الاردن"
      },
      {
        flag: "https://restcountries.eu/data/kwt.svg",
        countryCode: "+965",
        name: "Kuwait",
        name_ar: "الكويت"
      },
      {
        flag: "https://restcountries.eu/data/afg.svg",
        countryCode: "+93",
        name: "Afghanistan",
        name_ar: "افغانستان"
      },
      {
        flag: "https://restcountries.eu/data/jor.svg",
        countryCode: "+962",
        name: "Jordan",
        name_ar: "الاردن"
      },
      {
        flag: "https://restcountries.eu/data/kwt.svg",
        countryCode: "+965",
        name: "Kuwait",
        name_ar: "الكويت"
      },
      {
        flag: "https://restcountries.eu/data/afg.svg",
        countryCode: "+93",
        name: "Afghanistan",
        name_ar: "افغانستان"
      },
      {
        flag: "https://restcountries.eu/data/jor.svg",
        countryCode: "+962",
        name: "Jordan",
        name_ar: "الاردن"
      },
      {
        flag: "https://restcountries.eu/data/kwt.svg",
        countryCode: "+965",
        name: "Kuwait",
        name_ar: "الكويت"
      }
    ];
  }

  SharingMessage = new Subject(); 
}


