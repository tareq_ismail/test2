import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'strippedString'
})
export class StrippedStringPipe implements PipeTransform {

  transform(value: string) {
    return value.replace(/(<([^>]+)>)/gi, "");
  }

}
