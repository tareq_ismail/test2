import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cutTxt'
})
export class CutTxtPipe implements PipeTransform {

  transform(value: string, maxLength): unknown {
    if(value.length > maxLength) return value.substr(0,maxLength) + ".. <a href='#'>readmore</a>";
    return value;
  }

}
