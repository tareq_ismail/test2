import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatPrice'
})
export class FormatPricePipe implements PipeTransform {

  transform(value): any {
    return (Math.round(value * 100) / 100).toFixed(2);
  }

}
