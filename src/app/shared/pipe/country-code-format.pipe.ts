import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'countryCodeFormat'
})
export class CountryCodeFormatPipe implements PipeTransform {

  transform(value): any {
    return value.replace('+','00');
  }

}
