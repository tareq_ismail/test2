import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'initialsName'
})
export class InitialsNamePipe implements PipeTransform {

  transform(value: string): unknown {
    return value.split(/\s/).reduce((response,word)=> response+=word.slice(0,1),'').substring(0,2).toUpperCase()
  }

}
