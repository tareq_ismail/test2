import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatLocal'
})
export class FormatLocalPipe implements PipeTransform {

  transform(value): any {
    return value.toLocaleString();
  }

}
