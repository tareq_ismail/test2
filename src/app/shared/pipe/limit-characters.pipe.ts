import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'limitCharacters'
})
export class LimitCharactersPipe implements PipeTransform {

  transform(value: string, maxLength): unknown {
    if(value.length > maxLength) return value.substr(0,maxLength) + "..";
    return value;
  }

}
