import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent implements OnInit {
  @Input() id: string;
  @Input() url: string;
  @Input() img: string;
  @Input() title: string;
  @Input() isPlus: boolean;
  constructor() { }

  ngOnInit(): void {
  }

}
