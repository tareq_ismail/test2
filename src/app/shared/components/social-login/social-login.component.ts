import { Component, OnInit } from '@angular/core';
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { ApiService } from '../../services/api.service';
import { SocialAuthService } from "angularx-social-login";
import { LocalStorage as ls } from "../../../utils/localstorage.service";
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-social-login',
  templateUrl: './social-login.component.html',
  styleUrls: ['./social-login.component.scss']
})
export class SocialLoginComponent implements OnInit {

  constructor(
    private api: ApiService,
    private authService: SocialAuthService,
    private alert: AlertService
  ) { }


  ngOnInit(): void {
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(x => {
      this.api.socialLogin({provider: 'google', token:x.authToken}).subscribe((data:any) => {
        this.saveData(data.results);
      },
      (err) => {
        if(err.error.errors) this.alert.errorAPI(err.error.errors);
        else this.alert.error({ title: err.error.message });
      })
    });
  }

  saveData(obj){
    let currentUserModel = obj.user;
    currentUserModel.Authorization = "Bearer " + obj.access_token;
    ls.setValue("currentUser", currentUserModel);
    window.location.reload();
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID).then(x => {
      this.api.socialLogin({provider: 'facebook', token:x.authToken}).subscribe((data:any) => {
        // console.log(data);
        this.saveData(data.results);
      },
      (err) => {
        if(err.error.errors) this.alert.errorAPI(err.error.errors);
        else this.alert.error({ title: err.error.message });
      })
    });
  }

}
