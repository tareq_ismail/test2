import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../services/shared.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  footerMenu: any = [
    { title: "البرامج", url: "/program/all" },
    { title: "جدول البرامج", url: "/home" },
    { title: "اخبار", url: "/home" },
    { title: "التطبيقات", url: "/home" },
    { title: "اعلن معنا", url: "/home" },
    { title: "عن رؤيا", url: "/home" },
    { title: "مقدمو البرامج", url: "/home" },
    { title: "وظائف", url: "/home" },
    { title: "اتصل بنا", url: "/home" }
  ];


  constructor(
    private shared: SharedService
  ) { }

  ngOnInit(): void {

  }

  scrollToTop(){
    this.shared.scrollToTop();
  }

}
