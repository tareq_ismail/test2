import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DdlSearchComponent } from './ddl-search.component';

describe('DdlSearchComponent', () => {
  let component: DdlSearchComponent;
  let fixture: ComponentFixture<DdlSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DdlSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DdlSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
