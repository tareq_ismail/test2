import { EventEmitter, AfterViewInit, Component, Input, OnDestroy, OnInit, Output, ViewChild } from "@angular/core";
import { FormControl } from "@angular/forms";
import { MatSelect } from "@angular/material/select";
import { ReplaySubject, Subject } from "rxjs";
import { take, takeUntil } from "rxjs/operators";

@Component({
  selector: 'app-ddl-search',
  templateUrl: './ddl-search.component.html',
  styleUrls: ['./ddl-search.component.scss']
})

export class DdlSearchComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() options: any;
  @Input() placeholder: string;
  @Input() value: any;
  @Input() isSubmitted: boolean;
  @Input() isRequired: boolean;
  @Input() disabled: boolean;
  @Input() validationMsg: string;
  // 
  @Output() changeEmiter: EventEmitter<any> = new EventEmitter();

  public selectCtrl: FormControl = new FormControl();
  public searchFilterCtrl: FormControl = new FormControl();

  /** list of options filtered by search keyword */
  public filteredOptions: ReplaySubject<any> = new ReplaySubject<any>(1);

  @ViewChild("singleSelect", { static: true }) singleSelect: MatSelect;

  /** Subject that emits when the component has been destroyed. */
  protected _onDestroy = new Subject<void>();

  constructor() {
    
  }

  ngOnInit() {
    this.initialize();
    
  }

  ngOnChanges(changes) {
    this.initialize();
  }

  initialize(){
    this.placeholder = this.placeholder ? this.placeholder : "";
    // 
    // set selected value..
    let selectedIndex = this.value ? this.getSelectedIndex(this.value) : 0;
    this.selectCtrl.setValue(this.options[selectedIndex]);

    this.validationMsg = !this.validationMsg ? "Please_choose_one" : this.validationMsg;

    // load the initial option list
    this.filteredOptions.next(this.options.slice());

    // listen for search field value changes
    this.searchFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterOptions();
      });
      // 
      this.changeEmiter.emit(this.selectCtrl.value);
  }

  getSelectedIndex(val){
    for (let index = 0; index < this.options.length; index++) {
      if(this.options[index].id == val) return index;
    }
    return "";
  }

  ngAfterViewInit() {
    this.setInitialValue();
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  /**
   * Sets the initial value after the filteredOptions are loaded initially
   */
  protected setInitialValue() {
    this.filteredOptions
      .pipe(
        take(1),
        takeUntil(this._onDestroy)
      )
      .subscribe(() => {
        // setting the compareWith property to a comparison function
        // triggers initializing the selection according to the initial value of
        // the form control (i.e. _initializeSelection())
        // this needs to be done after the filteredOptions are loaded initially
        // and after the mat-option elements are available
        this.singleSelect.compareWith = (a, b) =>
          a && b && a.id === b.id;
      });
  }

  protected filterOptions() {
    if (!this.options) {
      return;
    }
    // get the search keyword
    let search = this.searchFilterCtrl.value;
    if (!search) {
      this.filteredOptions.next(this.options.slice());
      return;
    } else {
      search = search.toLowerCase();
    }
    // filter the options
    this.filteredOptions.next(
      this.options.filter(obj => obj.name.toLowerCase().indexOf(search) > -1)
    );
  }

  onChange(e){
    let obj = this.selectCtrl.value;
    obj["isValid"] = this.isRequired && !obj.id ? false : true;
    this.changeEmiter.emit(this.selectCtrl.value);
  }

}
