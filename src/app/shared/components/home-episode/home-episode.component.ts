import { Component, Input, OnChanges, OnInit } from '@angular/core';
import SwiperCore, { Virtual } from "swiper/core";

SwiperCore.use([Virtual]);

@Component({
  selector: 'app-home-episode',
  templateUrl: './home-episode.component.html',
  styleUrls: ['./home-episode.component.scss']
})
export class HomeEpisodeComponent implements OnInit {

  @Input() items ;
  @Input() title:string;
  @Input() className ;
  constructor() { }


  ngOnInit(): void {
  }
  onSwiper(e){

  }
  onSlideChange(){

  }
  config = {
    slidesPerView: 4.4,
    spaceBetween: 10,
    // Responsive breakpoints
    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 2.3,
        spaceBetween: 10
      },
      // when window width is >= 480px
      480: {
        slidesPerView: 2.3,
        spaceBetween: 10
      },
      // when window width is >= 640px
      750: {
        slidesPerView: 3.3,
        spaceBetween: 10
      },
      960: {
        slidesPerView: 4.3,
        spaceBetween: 10
      },
      1200: {
        slidesPerView: 4.3,
        spaceBetween: 10
      }
    }
  };
}
