import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeEpisodeComponent } from './home-episode.component';

describe('HomeEpisodeComponent', () => {
  let component: HomeEpisodeComponent;
  let fixture: ComponentFixture<HomeEpisodeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeEpisodeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeEpisodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
