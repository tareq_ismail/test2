import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { SharedService } from '../../services/shared.service';

@Component({
selector: 'app-header',
templateUrl: './header.component.html',
styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {
isProgramPage: boolean = false;
isSeriesPage: boolean = false;
// 
menu1: boolean = false;
menu2: boolean = false;
menu3: boolean = false;
userInfo: any = {};
// 
mainMenu: any = [
  { title: "الرئيسية", url: "/home" },
  { title: "برامج", url: "/program/all" },
  { title: "مسلسلات", url: "/series/all" },
  { title: "جدول البرامج", url: "/home" },
  { title: "أخبار", url: "/home" },
  { title: "التطبيقات", url: "/home" },
  { title: "أعلن معنا", url: "/home" },
  { title: "عن رؤيا", url: "/home" },
  { title: "مقدمو البرامج", url: "/home" },
  { title: "وظائف", url: "/home" }
];
// 
secondList: any = [];
// 
constructor(
  private shared: SharedService,
  private router: Router,
) {
}

ngOnInit(): void {
  this.router.events.subscribe((event) => {
    if (event instanceof NavigationEnd) {
      this.initializeData();
    }
  });
}

initializeData(){
  this.isProgramPage = window.location.href.indexOf("program") !== -1;
  this.isSeriesPage = window.location.href.indexOf("series") !== -1;
  // 
  // this.userInfo = {//in case vip subscription
  //   id: 1,
  //   username: "فارس",
  //   img: "assets/images/default-img.gif",
  //   royaplusSubscription: {
  //     annual_price: 18.99,
  //     subscription_start: "20/11/2020",
  //     subscription_end: "20/11/2021"
  //   }
  // };
  this.userInfo = {//in case free subscription
    id: 1,
    username: "فارس",
    img: "assets/images/default-img.gif",
    royaplusSubscription: false
  };

  if(this.isProgramPage){
    this.secondList = [
      { title: "الكل", url: "/program/all" }, //is not an option // its required..
      { title: "كوميديا", url: "/program/comedy" },
      { title: "طبخ", url: "/program/cooking" },
      { title: "منوعات", url: "/program/varieties" },
      { title: "أطفال", url: "/program/children" },
      { title: "موسيقى", url: "/program/music" },
      { title: "وثائقي", url: "/program/documentary" }
    ];
  }else if(this.isSeriesPage){
    this.secondList = [
      { title: "الكل", url: "/series/all" }, //is not an option // its required..
      { title: "سوري", url: "/series/syrian" },
      { title: "أردني", url: "/series/jordanian" },
      { title: "لبناني", url: "/series/lebanese" },
      { title: "مصري", url: "/series/egyptian" }
    ];
  }


}

isLoggedIn(){
  return !this.shared.isEmpty(this.userInfo);
}

menuItemIsActive(item){
  let c_item = window.location.href;
  if(c_item.indexOf("/program/") !== -1) c_item = "/program";
  if(c_item.indexOf("/series/") !== -1) c_item = "/series";
  if(c_item.indexOf("/home") !== -1) c_item = "/home";
  // 
  return item.url.indexOf(c_item) !== -1;
}

secondListIsActive(item){
  return window.location.href.indexOf(item.url) !== -1;
}


menuClick(order){
  if(order == 1){
    this.menu1 = true;
    this.menu2 = false;
    this.menu3 = false;
  }else if(order == 2){
    this.menu1 = false;
    this.menu2 = true;
    this.menu3 = false;      
  }else if(order == 3){
    this.menu1 = false;
    this.menu2 = false;
    this.menu3 = true;
  }
}

}
