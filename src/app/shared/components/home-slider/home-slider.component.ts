import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-slider',
  templateUrl: './home-slider.component.html',
  styleUrls: ['./home-slider.component.scss']
})
export class HomeSliderComponent implements OnInit {
  
  @Input() items ;
  @Input() title:string;
  @Input() tabs ;
  @Input() items2 ;

  data: any;
  constructor() { }


  ngOnInit(): void {
    this.data = this.items;
  }
  selectTab(item){
    this.tabs.forEach(function(x){
      if(item.id == x.id){
        x.active= true;
      }
      else{
        x.active = false;
      }
    })
    this.data = this[item.data];
  }
  onSwiper(e){

  }
  onSlideChange(){

  }
  config = {
    slidesPerView: 6,
    spaceBetween: 10,
    // Responsive breakpoints
    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 2.3,
        spaceBetween: 10
      },
      // when window width is >= 480px
      480: {
        slidesPerView: 2.3,
        spaceBetween: 10
      },
      // when window width is >= 640px
      750: {
        slidesPerView: 4,
        spaceBetween: 10
      },
      960: {
        slidesPerView: 6,
        spaceBetween: 10
      },
      1200: {
        slidesPerView: 6,
        spaceBetween: 10
      }
    }
  };

}
