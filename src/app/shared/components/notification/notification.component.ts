import { SharedService } from './../../services/shared.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
  message: any = "";
  showMessage: any = false;
  constructor(
    private shared: SharedService
    ) {
    this.shared.SharingMessage.subscribe(
      (res :any) => {
        this.showMessage = true;
        this.message = res.message;
      }
    )
   }

  ngOnInit(): void {
  }

}
