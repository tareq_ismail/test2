import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[DigitInput]'
})
export class DigitInputDirective {

  constructor() { }

  @HostListener('keypress', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    if(!(event.key === 'Enter' || event.keyCode === 13)){
      return this.isDigit(event);      
    }
   }

   isDigit(event){
    const charCode = (event.which) ? event.which : event.keyCode;    
    if(charCode == "46") return true; //if he press a dot button..
    if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;
    return true;
  }

}