import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[stopBubble]'
})
export class StopBubbleDirective {

  constructor() { }

  @HostListener('click', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    event.stopPropagation();
  }

}