import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { DigitInputDirective } from './directives/digit-input.directive';
import { StopBubbleDirective } from './directives/stop-bubble.directive';
import { FormatPricePipe } from './pipe/format-price.pipe';
import { SanitizeUrlPipe } from './pipe/sanitize-url.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { CutTxtPipe } from './pipe/cut-txt.pipe';
import { Ng2IziToastModule } from 'ng2-izitoast';
import { NgxSpinnerModule } from "ngx-spinner";
import { LoaderComponent } from './components/loader/loader.component';
import { AdComponent } from './components/ad/ad.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { DdlSearchComponent } from './components/ddl-search/ddl-search.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NgxDropdownMenuSearchModule } from 'ngx-dropdown-menu-search';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { SearchPipe } from './pipe/search.pipe';
import { MoveNextByMaxLengthDirective } from './directives/move-next-by-max-length.directive';
import { HomeEpisodeComponent } from './components/home-episode/home-episode.component';
import { SwiperModule } from 'swiper/angular';
import { HomeSliderComponent } from './components/home-slider/home-slider.component';
import { StoryComponent } from '../layout/components/story/story.component';
import { NotificationComponent } from './components/notification/notification.component';
import { CountryCodeFormatPipe } from './pipe/country-code-format.pipe';
// 
@NgModule({
  declarations: [HeaderComponent, FooterComponent, DigitInputDirective, StopBubbleDirective, FormatPricePipe, SanitizeUrlPipe, CutTxtPipe, LoaderComponent, AdComponent, ListItemComponent, SearchPipe, DdlSearchComponent, MoveNextByMaxLengthDirective, StoryComponent, HomeEpisodeComponent,NotificationComponent, HomeSliderComponent, CountryCodeFormatPipe],
  imports: [CommonModule, RouterModule, NgbModule, FormsModule, Ng2IziToastModule, NgxSpinnerModule, ReactiveFormsModule, NgxMatSelectSearchModule, MatFormFieldModule, MatSelectModule, NgxDropdownMenuSearchModule, SwiperModule],
  exports: [HeaderComponent, FooterComponent, DigitInputDirective, FormatPricePipe, SanitizeUrlPipe, Ng2IziToastModule, LoaderComponent, ListItemComponent, ReactiveFormsModule, SearchPipe, DdlSearchComponent, NgxMatSelectSearchModule, MatFormFieldModule, MatSelectModule, NgxDropdownMenuSearchModule,MoveNextByMaxLengthDirective, StoryComponent, HomeEpisodeComponent,NotificationComponent,HomeSliderComponent,CountryCodeFormatPipe]
})
export class SharedModule {}
