import { Component, Input, OnInit, Output, ViewChild } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { SharedService } from 'src/app/shared/services/shared.service';
import { HostListener } from '@angular/core';
import { NgbCarousel } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.scss']
})
export class StoryComponent implements OnInit {
  @Input() flag: boolean;
  @Input() data: any;
  @Output() toggleEmiter: EventEmitter<any> = new EventEmitter();
  sliderClass: string = "";
  // 
  slider: any = [];
  interval: number = 15000;
  sliderWidth: number = 0.0;
  // 
  @ViewChild('storyCarousel') storyCarousel: NgbCarousel;
  enableSlider: boolean = false;
  //
  constructor(private shared: SharedService) { }

  ngOnInit(): void {
    // console.log(this.storyCarousel.animation);
    
    this.flag = this.shared.stringToBoolean(this.flag);
    this.toggleBodyOverflow(this.flag ? true : false);
    this.getData();
    //
    this.sliderWidth = window.innerHeight / 2;
  }

  getData(){
    this.slider = this.data?.stories;
    this.setPaginationClass(1);
  }

  ngOnChanges(changes) {
    this.flag = this.shared.stringToBoolean(this.flag);
    this.toggleBodyOverflow(this.flag ? true : false);
  }

  toggle(){
    this.flag = !this.flag;  
    this.toggleEmiter.emit({flag:this.flag});
    this.toggleBodyOverflow(this.flag ? true : false);
  }

  close(){
    this.flag = false;
    this.toggleEmiter.emit({flag:this.flag});
    this.toggleBodyOverflow(this.flag ? true : false);
  }

  toggleBodyOverflow(flag){
    let body_ele = document.getElementsByTagName("body")[0];
    if(flag){
      body_ele.classList.add("overflow-hidden");
      this.enableSlider = true;
      this.setPaginationClass(1);
      this.startProgress(1);
    }else{
      body_ele.classList.remove("overflow-hidden");
      this.enableSlider = false;
    }
  }

  beforeSlideChange(e){
    let currentSlide = parseInt(e?.current.replace("ngb-slide-",""));
    let prevSlide = parseInt(e?.prev.replace("ngb-slide-",""));
    let totalSlider = this.slider.length - 1;
    let source = e.source;
    //  
    if(prevSlide == 0 && currentSlide == totalSlider && source == "arrowLeft"){
      this.storyCarousel.next();
    }else if((currentSlide == 0 && prevSlide == totalSlider) && (source == "arrowRight" || source == "timer")){
      this.close();
    }
    this.setPaginationClass(currentSlide + 1); 
    this.startProgress(currentSlide + 1);
  }

  startProgress(currentSlide){
    let $this = this;
    let querySelector = `.carousel-indicators > li:nth-child(${currentSlide})`;
    setTimeout(() => {
      let ele = (document.querySelector(querySelector) as HTMLElement);
      if(!ele) return false;
      ele.innerHTML = "<span></span>";
      let innerEle = (document.querySelector(`${querySelector} span`) as HTMLElement);
      let eachTime = 100;
      let totalTime = 0;

      var intervalId = setInterval(function() {
        totalTime+= eachTime;
        innerEle.style.width = (totalTime / $this.interval) * 100 + "%";
        if(totalTime == $this.interval) ele.innerHTML = "";
      }, eachTime);
      
      setTimeout(function(){
          clearInterval(intervalId);
      }, this.interval);
    });
  }

  setPaginationClass(c_slide){
    let eachCellWidth = 100 / this.slider.length;
    this.sliderClass = "";
    if(c_slide == 1) this.sliderClass = "first_slide";
    else if(c_slide == this.slider.length) this.sliderClass = "last_slide";
    // 
    setTimeout(() => {
      let carouselIndicators = document.querySelectorAll(".carousel-indicators > li");
      for (let index = 0; index < carouselIndicators.length; index++) {
        carouselIndicators[index].setAttribute("style","width:" + eachCellWidth + "%");
      }
    });
  }

  @HostListener('document:keyup', ['$event'])
  handleKeyboardEvent(event: KeyboardEvent) {
    if(event.key.toLowerCase() == "escape"){
      this.close();
    }
  }  

}
