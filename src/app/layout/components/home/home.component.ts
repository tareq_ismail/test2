import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  stories: any = [
    {
      title: "سيرين عبد النور",
      img: "assets/images/users/1.jpg",
      isToggled: false,
      stories: [
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://miro.medium.com/max/2716/1*-XWivw_W631IldZ5ZmIbyA.jpeg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://miro.medium.com/max/2716/1*-XWivw_W631IldZ5ZmIbyA.jpeg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://miro.medium.com/max/2716/1*-XWivw_W631IldZ5ZmIbyA.jpeg"
        }
      ]
    },
    {
      title: "آخر اغاني عمر",
      img: "assets/images/users/2.jpg",
      isToggled: false,
      stories: [
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/fd/5a/6c/fd5a6cbab67c7c1409202eb829540601.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/a6/90/78/a690789dec7274b03686bc62421d64db.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/736x/71/f5/73/71f573a5f84c9ac5e0e8c5ea9e5f0c66.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/fd/5a/6c/fd5a6cbab67c7c1409202eb829540601.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/a6/90/78/a690789dec7274b03686bc62421d64db.jpg"
        }
      ]
    },
    {
      title: "الفنانين والنوم",
      img: "assets/images/users/3.jpg",
      isToggled: false,
      stories: [
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/fd/5a/6c/fd5a6cbab67c7c1409202eb829540601.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/a6/90/78/a690789dec7274b03686bc62421d64db.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/736x/71/f5/73/71f573a5f84c9ac5e0e8c5ea9e5f0c66.jpg"
        }
      ]
    },
    {
      title: "اسماء المنور",
      img: "assets/images/users/4.jpg",
      isToggled: false,
      stories: [
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/fd/5a/6c/fd5a6cbab67c7c1409202eb829540601.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/a6/90/78/a690789dec7274b03686bc62421d64db.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/736x/71/f5/73/71f573a5f84c9ac5e0e8c5ea9e5f0c66.jpg"
        }
      ]
    },
    {
      title: "صابر الرباعي",
      img: "assets/images/users/5.jpg",
      isToggled: false,
      stories: [
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/fd/5a/6c/fd5a6cbab67c7c1409202eb829540601.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/a6/90/78/a690789dec7274b03686bc62421d64db.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/736x/71/f5/73/71f573a5f84c9ac5e0e8c5ea9e5f0c66.jpg"
        }
      ]
    },
    {
      title: "سيرين عبد النور",
      img: "assets/images/users/6.jpg",
      isToggled: false,
      stories: [
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/fd/5a/6c/fd5a6cbab67c7c1409202eb829540601.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/a6/90/78/a690789dec7274b03686bc62421d64db.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/736x/71/f5/73/71f573a5f84c9ac5e0e8c5ea9e5f0c66.jpg"
        }
      ]
    },
    {
      title: "آخر اغاني ادهم",
      img: "assets/images/users/7.jpg",
      isToggled: false,
      stories: [
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/fd/5a/6c/fd5a6cbab67c7c1409202eb829540601.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/a6/90/78/a690789dec7274b03686bc62421d64db.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/736x/71/f5/73/71f573a5f84c9ac5e0e8c5ea9e5f0c66.jpg"
        }
      ]
    },
    {
      title: "الفنانين والنوم",
      img: "assets/images/users/8.jpg",
      isToggled: false,
      stories: [
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/fd/5a/6c/fd5a6cbab67c7c1409202eb829540601.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/a6/90/78/a690789dec7274b03686bc62421d64db.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/736x/71/f5/73/71f573a5f84c9ac5e0e8c5ea9e5f0c66.jpg"
        }
      ]
    },
    {
      title: "اسماء المنور",
      img: "assets/images/users/9.jpg",
      isToggled: false,
      stories: [
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/fd/5a/6c/fd5a6cbab67c7c1409202eb829540601.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/originals/a6/90/78/a690789dec7274b03686bc62421d64db.jpg"
        },
        {
          title: 'في "أماكن السهر".. عمرو دياب يحكي قصة حبه مع دينا الشربيني كوكب الاردن - يعرض الآن الحلق',
          img: "https://i.pinimg.com/736x/71/f5/73/71f573a5f84c9ac5e0e8c5ea9e5f0c66.jpg"
        }
      ]
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
