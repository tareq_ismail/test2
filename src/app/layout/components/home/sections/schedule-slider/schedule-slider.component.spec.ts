import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduleSliderComponent } from './schedule-slider.component';

describe('ScheduleSliderComponent', () => {
  let component: ScheduleSliderComponent;
  let fixture: ComponentFixture<ScheduleSliderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduleSliderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScheduleSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
