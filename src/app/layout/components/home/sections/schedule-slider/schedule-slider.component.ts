import { Component, OnInit } from '@angular/core';
// import Swiper core and required modules
import SwiperCore, {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
} from 'swiper/core';

// install Swiper modules
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);
                         
@Component({
  selector: 'app-schedule-slider',
  templateUrl: './schedule-slider.component.html',
  styleUrls: ['./schedule-slider.component.scss']
})
export class ScheduleSliderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onSwiper(swiper) {
    console.log(swiper);
  }
  onSlideChange() {
    console.log('slide change');
  }

  buildURL(id){
    return "#"; 
  }

  items = [
    {
      id: 4,
      img: "../assets/images/new_year.png",
      title: "سهرتنا اليوم في قناة رؤيا",
      sub_title: "احتفال السنة الجديدة 2021",
      hint:"تشويق",
      url: this.buildURL(4),
      isPlus: true,
      live: true
    },
    {
      id: 1,
      img: "https://img.roya.tv/imageserv/Size208Q70/images/programs/74/aBYoOffEPfyZrEiHJvkzi2qsUZvBEMwiD26xOWMe.jpeg",
      title: "تشويش واضح -الحلقة 26",
      sub_title: "الموسم الاول.",
      hint:"تشويق",
      url: this.buildURL(1),
      isPlus: false,
    },
    {
      id: 2,
      img: "https://img.roya.tv/imageserv/Size1200Q70/episodes/stream/1081/D3WjostBrHavF4NfBKQ1bhSmR5CVLkrHErljMABr.png",
      title: "جمع سالم - الحلقة العاشرة",
      sub_title: "الموسم الاول.",
      hint:"تشويق",
      url: this.buildURL(2),
      isPlus: false
    },
    {
      id: 3,
      img: "https://img.roya.tv/imageserv/Size400Q70/images/articles//05NwBFEm0BjDeIGFlWce5t8PRjQnAKAxzNS8VRlb.jpeg",
      title: "سفرات وبهارات",
      sub_title: "الموسم الاول.",
      hint:"تشويق",
      url: this.buildURL(3),
      isPlus: false
    }
  ];
}
