import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveProgramsComponent } from './live-programs.component';

describe('LiveProgramsComponent', () => {
  let component: LiveProgramsComponent;
  let fixture: ComponentFixture<LiveProgramsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LiveProgramsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveProgramsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
