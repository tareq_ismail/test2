import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoyaAppComponent } from './roya-app.component';

describe('RoyaAppComponent', () => {
  let component: RoyaAppComponent;
  let fixture: ComponentFixture<RoyaAppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoyaAppComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoyaAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
