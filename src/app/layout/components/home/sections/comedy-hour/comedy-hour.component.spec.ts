import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComedyHourComponent } from './comedy-hour.component';

describe('ComedyHourComponent', () => {
  let component: ComedyHourComponent;
  let fixture: ComponentFixture<ComedyHourComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComedyHourComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComedyHourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
