import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoyaKitchenComponent } from './roya-kitchen.component';

describe('RoyaKitchenComponent', () => {
  let component: RoyaKitchenComponent;
  let fixture: ComponentFixture<RoyaKitchenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoyaKitchenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoyaKitchenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
