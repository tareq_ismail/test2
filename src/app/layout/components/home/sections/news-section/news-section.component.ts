import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-news-section',
  templateUrl: './news-section.component.html',
  styleUrls: ['./news-section.component.scss']
})
export class NewsSectionComponent implements OnInit {
  data: any = []
  constructor() { }

  ngOnInit(): void {
  }
  selectTab(item){
    this.tabs.forEach(function(x){
      if(item.id == x.id){
        x.active= true;
      }
      else{
        x.active = false;
      }
    })
    this.data = this[item.data];
  }
  tabs = [
    {
      id:1,
      title:"الكل",
      active: true,
      data: "items"
    },
    {
      id:2,
      title:"فن ومشاهير",
      active: false,
      data: "items2"
    },
    {
      id:3,
      title:"منوعات",
      active: false,
      data: "items2"
    },
    {
      id:4,
      title:"دراما وسينما",
      active: false,
      data: "items2"
    },
    {
      id:5,
      title:"رمضان 2020",
      active: false,
      data: "items2"
    },
    {
      id:6,
      title:"توب 5",
      active: false,
      data: "items2"
    },
    {
      id:7,
      title:"الابراج",
      active: false,
      data: "items2"
    }
  ]

}
