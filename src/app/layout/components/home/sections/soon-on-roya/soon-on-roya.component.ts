import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-soon-on-roya',
  templateUrl: './soon-on-roya.component.html',
  styleUrls: ['./soon-on-roya.component.scss']
})
export class SoonOnRoyaComponent implements OnInit {
  title="قريبا على قناة رؤيا"
  items = [
    {
      id: 1,
      img: "https://img.roya.tv/imageserv/Size1920Q70/programs/stream/T415X1K0NBLsSWgILa8mkxuMcWnhVDigynSqFaO1.jpeg",
      title: "من الأخر",
      number: 5,
      date: "2020 .11 .20",
      url: this.buildURL(1),
      isPlus: true,
      isVedio: false
    },
    {
      id: 2,
      img: "https://img.roya.tv/imageserv/Size1200Q70/episodes/stream/1081/D3WjostBrHavF4NfBKQ1bhSmR5CVLkrHErljMABr.png",
      title: "جمع سالم",
      number: 10,
      date: "2020 .11 .20",
      url: this.buildURL(2),
      isPlus: true,
      isVedio: false
    }
  ];
  constructor() { }

  ngOnInit(): void {
  }

  buildURL(n){
    return '#';
  }

  onSwiper(e){

  }
  onSlideChange(){

  }

}
