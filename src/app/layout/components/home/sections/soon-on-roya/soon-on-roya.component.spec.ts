import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SoonOnRoyaComponent } from './soon-on-roya.component';

describe('SoonOnRoyaComponent', () => {
  let component: SoonOnRoyaComponent;
  let fixture: ComponentFixture<SoonOnRoyaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SoonOnRoyaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoonOnRoyaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
