import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CaravanPlusComponent } from './caravan-plus.component';

describe('CaravanPlusComponent', () => {
  let component: CaravanPlusComponent;
  let fixture: ComponentFixture<CaravanPlusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CaravanPlusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CaravanPlusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
