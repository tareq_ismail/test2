import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-caravan-plus',
  templateUrl: './caravan-plus.component.html',
  styleUrls: ['./caravan-plus.component.scss']
})
export class CaravanPlusComponent implements OnInit {

  
  title:string = "كرفان PLUS"
  items = [
        {
          id: 1,
          img: "https://img.roya.tv/imageserv/Size208Q70/images/programs/74/aBYoOffEPfyZrEiHJvkzi2qsUZvBEMwiD26xOWMe.jpeg",
          title: "تشويش واضح -الحلقة 26",
          url: this.buildURL(1),
          isPlus: false
        },
        {
          id: 2,
          img: "https://img.roya.tv/imageserv/Size1200Q70/episodes/stream/1081/D3WjostBrHavF4NfBKQ1bhSmR5CVLkrHErljMABr.png",
          title: "جمع سالم - الحلقة العاشرة",
          url: this.buildURL(2),
          isPlus: false
        },
        {
          id: 3,
          img: "https://img.roya.tv/imageserv/Size400Q70/images/articles//05NwBFEm0BjDeIGFlWce5t8PRjQnAKAxzNS8VRlb.jpeg",
          title: "سفرات وبهارات",
          url: this.buildURL(3),
          isPlus: false
        },
        {
          id: 4,
          img: "https://img.roya.tv/imageserv/Size1920Q70/programs/stream/T415X1K0NBLsSWgILa8mkxuMcWnhVDigynSqFaO1.jpeg",
          title: "من الاخر",
          url: this.buildURL(4),
          isPlus: true
        },
        {
          id: 5,
          img: "https://upload.wikimedia.org/wikipedia/ar/e/e1/%D8%BA%D9%84%D8%A7%D9%81_%D9%85%D8%B3%D9%84%D8%B3%D9%84_%D8%A7%D9%84%D9%87%D9%8A%D8%A8%D8%A9_%D8%A7%D9%84%D8%B9%D9%88%D8%AF%D8%A9.jpeg",
          title: "الهيبة",
          url: this.buildURL(5),
          isPlus: false
        },
        {
          id: 6,
          img: "https://www.companyfolders.com/blog/media/2015/04/million-dollar-arm-poster.jpg",
          title: "كوكب الأردن يعرض الآن الحلقة 10",
          url: this.buildURL(6),
          isPlus: true
        },
        {
          id: 5,
          img: "https://upload.wikimedia.org/wikipedia/ar/e/e1/%D8%BA%D9%84%D8%A7%D9%81_%D9%85%D8%B3%D9%84%D8%B3%D9%84_%D8%A7%D9%84%D9%87%D9%8A%D8%A8%D8%A9_%D8%A7%D9%84%D8%B9%D9%88%D8%AF%D8%A9.jpeg",
          title: "الهيبة",
          url: this.buildURL(5),
          isPlus: false
        },
        {
          id: 6,
          img: "https://www.companyfolders.com/blog/media/2015/04/million-dollar-arm-poster.jpg",
          title: "كوكب الأردن يعرض الآن الحلقة 10",
          url: this.buildURL(6),
          isPlus: true
        }
      ];
  constructor() { }

  ngOnInit(): void {
  }
  buildURL(id){
    return "#"; 
  }

}
