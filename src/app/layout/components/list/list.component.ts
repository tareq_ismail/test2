import { Component, OnChanges, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  isProgramPage: boolean = false;
  isSeriesPage: boolean = false;
  category: any = "";
  // 
  items: any = [];

  constructor(
    private router: Router,
    private route: ActivatedRoute
    ) { }


  ngOnInit(): void {
    this.initializeData();
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.initializeData(); 
      }
    });    
  }

  initializeData(){
    this.isProgramPage = window.location.href.indexOf("program") !== -1;
    this.isSeriesPage = window.location.href.indexOf("series") !== -1;            
    this.category = this.route.snapshot.paramMap.get('categoryType')?.toString();    
    // 
    this.items = [
      {
        id: 1,
        img: "https://i.pinimg.com/originals/96/a0/0d/96a00d42b0ff8f80b7cdf2926a211e47.jpg",
        title: "كوكب الأردن يعرض الآن الحلقة 10",
        url: this.buildURL(1),
        isPlus: false
      },
      {
        id: 2,
        img: "https://images-na.ssl-images-amazon.com/images/I/61Zf5g-xUxL._AC_SL1039_.jpg",
        title: "كوكب الأردن يعرض الآن الحلقة 10",
        url: this.buildURL(2),
        isPlus: false
      },
      {
        id: 3,
        img: "https://images.moviepostershop.com/replicas-movie-poster-1000778791.jpg",
        title: "كوكب الأردن يعرض الآن الحلقة 10",
        url: this.buildURL(3),
        isPlus: false
      },
      {
        id: 4,
        img: "https://i.pinimg.com/originals/57/78/da/5778da960c0bf6fe2cfe1621ccd53c38.jpg",
        title: "كوكب الأردن يعرض الآن الحلقة 10",
        url: this.buildURL(4),
        isPlus: true
      },
      {
        id: 5,
        img: "https://images.moviepostershop.com/rust-creek-movie-poster-1000778781.jpg",
        title: "كوكب الأردن يعرض الآن الحلقة 10",
        url: this.buildURL(5),
        isPlus: false
      },
      {
        id: 6,
        img: "https://www.companyfolders.com/blog/media/2015/04/million-dollar-arm-poster.jpg",
        title: "كوكب الأردن يعرض الآن الحلقة 10",
        url: this.buildURL(6),
        isPlus: true
      },
      {
        id: 7,
        img: "https://www.vintagemovieposters.co.uk/wp-content/uploads/2019/09/IMG_2911.jpeg",
        title: "كوكب الأردن يعرض الآن الحلقة 10",
        url: this.buildURL(7),
        isPlus: false
      },
      {
        id: 8,
        img: "https://cdn.shopify.com/s/files/1/0290/5663/0868/products/businessasusual999x666-min_1200x.jpg?v=1584637332",
        title: "كوكب الأردن يعرض الآن الحلقة 10",
        url: this.buildURL(8),
        isPlus: false
      },
      {
        id: 9,
        img: "https://image-cdn.hypb.st/https%3A%2F%2Fhypebeast.com%2Fimage%2F2020%2F03%2Fmonster-hunter-milla-jovovich-tony-jaa-movie-poster-info-1.jpg?q=75&w=800&cbr=1&fit=max",
        title: "كوكب الأردن يعرض الآن الحلقة 10",
        url: this.buildURL(9),
        isPlus: false
      },
      {
        id: 10,
        img: "https://chargefield.com/wp-content/uploads/2020/08/poster-ouas.jpg",
        title: "كوكب الأردن يعرض الآن الحلقة 10",
        url: this.buildURL(10),
        isPlus: false
      },
      {
        id: 11,
        img: "https://m.media-amazon.com/images/M/MV5BM2RiYTM3NjAtNDUyOC00OWFhLWE3ZGEtNjkzNzI5YmE1M2E5XkEyXkFqcGdeQXVyMDM2NDM2MQ@@._V1_.jpg",
        title: "كوكب الأردن يعرض الآن الحلقة 10",
        url: this.buildURL(11),
        isPlus: false
      },
      {
        id: 12,
        img: "https://i.redd.it/d2ax3bwvxdw51.jpg",
        title: "كوكب الأردن يعرض الآن الحلقة 10",
        url: this.buildURL(12),
        isPlus: true
      }
    ];

    // 
    if(this.isProgramPage) console.log("Program Page ===> " + this.category);
    if(this.isSeriesPage) console.log("Series Page =====> " + this.category);
    // 
  }

  buildURL(id){
    if(this.isProgramPage) return `/program-details/${id}`;
    else if(this.isSeriesPage) return `/series-details/${id}`;
    else return "#"; // will not used..
  }

}
