import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { LayoutRoutingModule } from './layout-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { HomeComponent } from './components/home/home.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { LayoutComponent } from './layout.component';
import { ListComponent } from './components/list/list.component';
import { SwiperModule } from 'swiper/angular';
import { KeepWatchingComponent } from './components/home/sections/keep-watching/keep-watching.component';
import { LatestEpisodesComponent } from './components/home/sections/latest-episodes/latest-episodes.component';
import { NewProgramsComponent } from './components/home/sections/new-programs/new-programs.component';
import { SoonOnRoyaComponent } from './components/home/sections/soon-on-roya/soon-on-roya.component';
import { LiveProgramsComponent } from './components/home/sections/live-programs/live-programs.component';
import { SeriesComponent } from './components/home/sections/series/series.component';
import { ScheduleSliderComponent } from './components/home/sections/schedule-slider/schedule-slider.component';
import { RoyaAppComponent } from './components/home/sections/roya-app/roya-app.component';
import { CaravanPlusComponent } from './components/home/sections/caravan-plus/caravan-plus.component';
import { ComedyHourComponent } from './components/home/sections/comedy-hour/comedy-hour.component';
import { RoyaKitchenComponent } from './components/home/sections/roya-kitchen/roya-kitchen.component';
import { RamadanComponent } from './components/home/sections/ramadan/ramadan.component';
import { MixComponent } from './components/home/sections/mix/mix.component';
import { DocumentaryComponent } from './components/home/sections/documentary/documentary.component';
import { ChildrenComponent } from './components/home/sections/children/children.component';
import { MusicComponent } from './components/home/sections/music/music.component';
import { NewsSectionComponent } from './components/home/sections/news-section/news-section.component';

@NgModule({
  declarations: [HomeComponent, LayoutComponent, ListComponent, ScheduleSliderComponent, KeepWatchingComponent, LatestEpisodesComponent, NewProgramsComponent, SoonOnRoyaComponent, LiveProgramsComponent, SeriesComponent, RoyaAppComponent, CaravanPlusComponent, ComedyHourComponent, RoyaKitchenComponent, RamadanComponent, MixComponent, DocumentaryComponent, ChildrenComponent, MusicComponent, NewsSectionComponent],
  imports: [CommonModule, SharedModule, LayoutRoutingModule, NgbModule, FormsModule, NgxIntlTelInputModule, NgxSpinnerModule,SwiperModule],
})
export class LayoutModule {}
