import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: () => import('./layout/layout.module').then((m) => m.LayoutModule), },
  { path: 'auth', loadChildren: () => import('./auth/auth-routing.module').then((m) => m.AuthRoutingModule), },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
