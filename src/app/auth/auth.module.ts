import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AuthRoutingModule } from './auth-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { SignInComponent } from './sign-in/sign-in.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { AuthComponent } from './auth.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ActivateAccountComponent } from './activate-account/activate-account.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';

@NgModule({
  declarations: [SignInComponent, AuthComponent, SignUpComponent, ActivateAccountComponent, ForgotPasswordComponent],
  imports: [CommonModule, SharedModule, AuthRoutingModule, NgbModule, FormsModule, NgxIntlTelInputModule, NgxSpinnerModule],
})
export class AuthModule { }