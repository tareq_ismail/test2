import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription, Observer } from 'rxjs';

@Component({
  selector: 'app-activate-account',
  templateUrl: './activate-account.component.html',
  styleUrls: ['./activate-account.component.scss']
})
export class ActivateAccountComponent implements OnInit {
  activateFormGroup: FormGroup;
  phoneNumber: any = "";
  showCounter: boolean = true;

  constructor(private formBuilder: FormBuilder, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.createForm();
    this.createFormControls()
    this.phoneNumber = this.route.snapshot.paramMap.get('phoneNumber');
  }

  counter = new Observable<string>((observer: Observer<string>) => {
    let count: any = 112;
    setInterval(() => {
      observer.next(count);
      if (count == 0) {
        this.showCounter = false;
        observer.complete()
      }

      count--;
    }, 1000)
  });

  createForm() {
    this.activateFormGroup = this.formBuilder.group({
      num_1: [null, Validators.required],
      num_2: [null, Validators.required],
      num_3: [null, Validators.required],
      num_4: [null, Validators.required],
    });
  }
  createFormControls() {
    return this.activateFormGroup.controls;
  }

  submit() {
    if (this.activateFormGroup.valid) {
      alert("done")
    }
  }

  keytab(e) {
    console.log(e)
  }

}
