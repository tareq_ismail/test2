import { SharedService } from './../../shared/services/shared.service';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  showPassord: boolean = false;
  showConfirmPassord: boolean = false;
  forgotPasswordFormGroup: FormGroup;
  constructor(private formBuilder: FormBuilder,private shared: SharedService) { }

  ngOnInit(): void {
    this.createForm();
    this.createFormControls();
  }

  createForm() {
    this.forgotPasswordFormGroup = this.formBuilder.group({
      password: [null, Validators.required],
      confirmPassword: [null, Validators.required],
    }, { validator: this.passwordConfirming });
  }

  passwordConfirming(frm: FormGroup) {
    return frm.controls['password'].value === frm.controls['confirmPassword'].value ? null : { 'mismatch': true };
  }
  createFormControls() {
    return this.forgotPasswordFormGroup.controls;
  }

  submit(){
    let data = {
      message: 'تم تغيير كلمة المرور بنجاح'
    }
    this.shared.SharingMessage.next(data);  
  }

}
