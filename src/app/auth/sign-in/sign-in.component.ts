import { SharedService } from './../../shared/services/shared.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit {
  phoneNumberSection: boolean = true;
  passswordSection: boolean = false;
  showPassord: boolean = false;
  selectedCountry: any = {};
  query: string = "";
  loginFormGroup: FormGroup;
  countryList: any = [];

  constructor(private formBuilder: FormBuilder, private shared: SharedService) { }


  ngOnInit(): void {
    this.createForm();
    this.createFormControls();
    this.countryList = this.shared.getCountryList();
    this.selectedCountry = {
      countryCode: "+962",
      flag: "https://restcountries.eu/data/jor.svg"
    }
  }

  createForm() {
    this.loginFormGroup = this.formBuilder.group({
      phone_number: [null, Validators.required],
      password: [null, Validators.required],
      query: [null]
    });
  }
  createFormControls() {
    return this.loginFormGroup.controls;
  }


  continue() {
    if (this.loginFormGroup.value["phone_number"]) {
      this.phoneNumberSection = false;
      this.passswordSection = true;
    }
  }

  selectCountry(country) {
    this.selectedCountry = {
      countryCode: country.countryCode,
      flag: country.flag
    }
  }

  goToForgotPassword(){
    this.shared.redirectTo("forgot-password")
  }



}
