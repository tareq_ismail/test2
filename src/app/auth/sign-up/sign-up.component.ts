import { SharedService } from './../../shared/services/shared.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  showPassord: boolean = false;
  selectedCountry: any = {};
  query: string = "";
  signupFormGroup: FormGroup;
  countryList: any = [];
  imagePath:any;
  url:any = "../../../assets/images/default-img.gif";
  constructor(
    private formBuilder: FormBuilder,
    private shared: SharedService,
    private cdr: ChangeDetectorRef
    ) { }

  ngOnInit(): void {
    this.createForm();
    this.createFormControls();
    this.countryList = this.shared.getCountryList();
    this.selectedCountry = {
      countryCode: "+962",
      flag: "https://restcountries.eu/data/jor.svg"
    }
  }

  createForm() {
    this.signupFormGroup = this.formBuilder.group({
      phone_number: [null, Validators.required],
      first_number: [null, Validators.required],
      last_number: [null, Validators.required],
      email: [null, Validators.required],
      password: [null, Validators.required],
      DOP: [null, Validators.required],
      query: [null]
    });
  }
  createFormControls() {
    return this.signupFormGroup.controls;
  }
  selectCountry(country) {
    this.selectedCountry = {
      countryCode: country.countryCode,
      flag: country.flag
    }
  }

  onFileChanged(event) {
    const files = event.target.files;
    if (files.length === 0)
        return;

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
        // this.message = "Only images are supported.";
        return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
        this.url = reader.result;
        this.cdr.detectChanges(); 
    }
}

  submit(){
    if(this.signupFormGroup.valid){
      this.shared.redirectTo('auth/activate-account',this.signupFormGroup.value.phone_number)
    }
  }


}
