// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  siteBaseURL: "https://dev-insurdoo-website.designinjo.com",
  baseURL: "https://dev-insurdoo.designinjo.com/api",
  GoogleLoginProviderClientID: "341031950198-rqeuqfhlth8nlu5767jj4lu100n2m78t.apps.googleusercontent.com",
  FacebookLoginProviderClientID: "723791378294383",
  defaultImg: "./assets/images/default-img.gif",
  shareLink: "https://dev-insurdoo-website.designinjo.com",
  // 
  maximumFileUpload: 3,//3 MG
  sourceOfTweet: "RoyaTV",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
