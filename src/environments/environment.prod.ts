export const environment = {
  production: true,
  siteBaseURL: "https://dev-insurdoo-website.designinjo.com",
  baseURL: "https://dev-insurdoo.designinjo.com/api",
  GoogleLoginProviderClientID: "341031950198-rqeuqfhlth8nlu5767jj4lu100n2m78t.apps.googleusercontent.com",
  FacebookLoginProviderClientID: "723791378294383",
  defaultImg: "./assets/images/default-img.gif",
  shareLink: "https://dev-insurdoo-website.designinjo.com",
  // 
  maximumFileUpload: 3,//3 MG
  sourceOfTweet: "RoyaTV",
};
